# wecodeit.academy
# cookie configuration inkl. linkedin Tags is in the database not in the code, however I saved [the basic snippet in the repo](CookieConsent.md).


## setup

```
# create a python3 virtualenv
virtualenv -p python3 venv

# activate the venv
source venv/bin/activate

# install all the backend requirements
pip install -r requirements.txt

# install all the frontend requirements
npm run install-deps

# run migrations like (will also create a sqlite database file)
python3 manage.py migrate

# create a super user
python3 manage.py createsuperuser

# start the dev server
python3 manage.py runserver

# login with the superuser and create your own database entries on:
http://127.0.0.1:8000/admin
```
