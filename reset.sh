echo "+++ remove database +++"
rm db.sqlite3
echo "+++ make migrations +++"
python manage.py makemigrations
echo "+++ migrate +++"
python manage.py migrate
echo "+++ create a new superuser +++"
python manage.py createsuperuser