from .base import *  # noqa
import dj_database_url

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'f!h%y2^l!wvu6#fli-c1s0n9nmn281hg#26%3o=^azkuq^m#=1'

ALLOWED_HOSTS = ['*']

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

WAGTAIL_CACHE = False

WAGTAILADMIN_BASE_URL = "127.0.0.1:8080"

DATABASES['default'] = dj_database_url.parse("postgres://wecodeit@localhost:5432/wecodeit", conn_max_age=600)

try:
    from .local_settings import *  # noqa
except ImportError:
    pass
