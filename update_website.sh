#!/bin/bash

# todo: check on (and maybe install) sys dependencies (npm and pip)

cd /var/www/wecodeit-webpage/

echo "+++ stop webserver service +++"
supervisorctl stop wecodeit-webpage

echo "+++ using the venv +++"
source venv/bin/activate

echo "+++ installing potentially changed backend requirements +++"
pip install -r requirements.txt

echo "+++ installing potentially changed frontend requirements +++"
npm run install-deps

echo "+++ template production settings +++"
npm run template-prod-settings

echo "+++ run migrations +++"
python3 ./manage.py migrate

echo "+++ compile SCSS files +++"
python3 ./manage.py compilescss

echo "+++ collect statics +++"
python3 ./manage.py collectstatic --noinput

echo "+++ start webserver server +++"
supervisorctl start wecodeit-webpage