var translations = ["ما برنامه نویسی میکنیم",  "نحن نبرمجه", "We code IT"];
var counter = -1;

function changeLogoText(){
  var logos = document.getElementsByClassName("logo-firstline");
  Array.from(logos).forEach(element => {
    element.style.opacity = "0" 
  });


  setTimeout(function(){
    counter++
    var iteration = (counter) % translations.length;
    Array.from(logos).forEach(element => {
      element.textContent = translations[iteration];
      element.style.opacity = "1";
    });
  }, 400); 
}

setTimeout(function(){
  setInterval(changeLogoText, 4000);
}, 2000);
