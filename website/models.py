"""
Creatable pages used in CodeRed CMS.
"""
from modelcluster.fields import ParentalKey
from coderedcms.forms import CoderedFormField
from django.db import models
from django.utils.translation import gettext_lazy as _
from coderedcms.models import (
    CoderedArticlePage,
    CoderedArticleIndexPage,
    CoderedEmail,
    CoderedFormPage,
    CoderedWebPage
)
from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from .blocks.wci_blocks import *
from coderedcms.blocks import *

WCI_STREAMBLOCKS = [
    ('hero', HeroBlock([
        ('row', GridBlock(CONTENT_STREAMBLOCKS +
                          [('timeline_block', TimeLineListBlock())])),
        ('cardgrid', CardGridBlock([
            ('card', CardBlock()),
        ])),
        ('html', blocks.RawHTMLBlock(icon='code',
                                     form_classname='monospace', label=_('HTML'))),
    ])),
    ('row', GridBlock(CONTENT_STREAMBLOCKS +
                      [('timeline_block', TimeLineListBlock())])),
    ('cardgrid', CardGridBlock([
        ('card', CardBlock()),
    ])),
    ('html', blocks.RawHTMLBlock(icon='code',
                                 form_classname='monospace', label=_('HTML'))),
]


class ArticlePage(CoderedArticlePage):
    """
    Article, suitable for news or blog content.
    """
    class Meta:
        verbose_name = 'Article'
        ordering = ['-first_published_at']

    # Only allow this page to be created beneath an ArticleIndexPage.
    parent_page_types = ['website.ArticleIndexPage']

    template = 'coderedcms/pages/article_page.html'
    amp_template = 'coderedcms/pages/article_page.amp.html'
    search_template = 'coderedcms/pages/article_page.search.html'


class ArticleIndexPage(CoderedArticleIndexPage):
    """
    Shows a list of article sub-pages.
    """
    class Meta:
        verbose_name = 'Article Landing Page'

    headline = models.TextField(
        blank=True,
        verbose_name=_("Headline")
    )
    sub_headline = models.TextField(
        blank=True,
        verbose_name=_("Subheadline")
    )
    content_panels = Page.content_panels + [
        FieldPanel('headline'),
        FieldPanel('sub_headline'),
        ImageChooserPanel('cover_image'),
    ]
    # Override to specify custom index ordering choice/default.
    index_query_pagemodel = 'website.ArticlePage'

    # Only allow ArticlePages beneath this page.
    subpage_types = ['website.ArticlePage']

    template = 'coderedcms/pages/article_index_page.html'
    body = StreamField(WCI_STREAMBLOCKS, null=True, blank=True)


class FormPage(CoderedFormPage):
    """
    A page with an html <form>.
    """
    class Meta:
        verbose_name = 'Form'

    template = 'coderedcms/pages/form_page.html'


class FormPageField(CoderedFormField):
    """
    A field that links to a FormPage.
    """
    class Meta:
        ordering = ['sort_order']

    page = ParentalKey('FormPage', related_name='form_fields')


class FormConfirmEmail(CoderedEmail):
    """
    Sends a confirmation email after submitting a FormPage.
    """
    page = ParentalKey('FormPage', related_name='confirmation_emails')


class WebPage(CoderedWebPage):
    """
    General use page with featureful streamfield and SEO attributes.
    """
    class Meta:
        verbose_name = 'Web Page'

    headline = models.TextField(
        blank=True,
        verbose_name=_("Headline")
    )
    sub_headline = models.TextField(
        blank=True,
        verbose_name=_("Subheadline")
    )
    content_panels = Page.content_panels + [
        FieldPanel('headline'),
        FieldPanel('sub_headline'),
        ImageChooserPanel('cover_image'),
    ]
    template = 'coderedcms/pages/web_page.html'
    body = StreamField(WCI_STREAMBLOCKS, null=True, blank=True)
