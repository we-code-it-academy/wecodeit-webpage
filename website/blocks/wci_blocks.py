from coderedcms.blocks import BaseBlock
from wagtail.core import blocks
from django.utils.translation import gettext_lazy as _


class TimeLineItemBlock(BaseBlock):
    """
    Represents one item in a Timeline.
    """

    date = blocks.CharBlock(
        required=True,
        label=_('Date')
    )
    year = blocks.CharBlock(
        required=True,
        label=_('Year')
    )
    content = blocks.RichTextBlock(label=_('Content'))

    class Meta:
        template = 'blocks/timelineitem_block.html'
        icon = 'fa-hourglass-half'
        label = _('TimelineItem')


class TimeLineListBlock(BaseBlock):
    """
    A Timeline, like on the .
    """
    heading = blocks.CharBlock(
        required=False,
        max_length=255,
        label=_('Heading'),
    )
    items = blocks.StreamBlock(
        [
            ('item', TimeLineItemBlock()),
        ],
        label=_('Milestones'),
    )

    class Meta:
        template = 'blocks/timeline_block.html'
        icon = 'fa-hourglass'
        label = _('Timeline')
